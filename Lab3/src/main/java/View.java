import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;
import javafx.scene.text.*;
import javafx.stage.Stage;

public class View extends Application{
    public Canvas canvas;
    private GraphicsContext gc;
    private final int size_x = 400, size_y = 800, dot_size = 40;
    private IntegerProperty s = new SimpleIntegerProperty(0);
    private String[] colors = new String[]{"Cyan", "Blue", "Orange", "Yellow", "Green", "Purple", "Red"};

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Controller c = new Controller(this);
        StackPane root = new StackPane();
        root.setStyle("-fx-background-color: Black");
        canvas = new Canvas(size_x,size_y);
        gc = canvas.getGraphicsContext2D();
        canvas.setFocusTraversable(true);
        root.getChildren().add(canvas);

        Label sc = new Label("Current score:");
        sc.setTextFill(Paint.valueOf("White"));
        sc.setFont(new Font("Verdana", 20));
        sc.setPadding(new Insets(5, 10, 0, 50));
        Label cur_score = new Label();
        cur_score.setPadding(new Insets(5));
        cur_score.setTextFill(Paint.valueOf("White"));
        cur_score.setFont(new Font("Verdana", 20));

        cur_score.textProperty().bind(s.asString());

        final MenuItem ng = new MenuItem("New Game");
        final MenuItem sb = new MenuItem("Scoreboard");
        final SeparatorMenuItem sep = new SeparatorMenuItem();
        final MenuItem ex = new MenuItem("Exit");
        final Menu g = new Menu("Game");
        g.getItems().addAll(ng, sb, sep, ex);
        ng.setOnAction(event -> c.new_game());
        ex.setOnAction(event -> System.exit(0));
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().add(g);
        HBox pane = new HBox(0, menuBar, sc, cur_score);
        root.getChildren().add(pane);

        c.start_game();
        Scene scene = new Scene(root, size_x, size_y);
        primaryStage.setTitle("TETRIS");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void draw(int[][] t)
    {
        gc.clearRect(0,0, size_x, size_y);
        for (int i = 0; i < size_x; i++)
        {
            for (int j = 0; j < size_y; j++)
            {
                if (t[i][j] != 0)
                {
                    gc.setFill(Paint.valueOf(colors[t[i][j] - 1]));
                    gc.fillRect(i, j, dot_size, dot_size);
                }

            }
        }
    }
    public void game_over()
    {
        gc.setFill(Paint.valueOf("#9EE9F8"));
        gc.setFont(new Font("Verdana", 50));
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText("Game Over", size_x/2, size_y/2);
    }
    public void setScore(int sc)
    {
        Platform.runLater(() -> s.set(sc));
    }
}
