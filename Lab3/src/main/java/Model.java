import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Model {
    private final int size_x = 400, size_y = 800, block_size = 40;
    private boolean game_over = false;
    private int table[][] = new int[size_x][size_y];
    private int score = 0;

    private ArrayList<Point> cur_obj = new ArrayList<>();
    private ArrayList<Point> tmp = new ArrayList<>();
    private final int[][][][] states = {
            {
                    {{0, 0}, {block_size, 0}, {2 * block_size, 0}, {3 * block_size, 0}},
                    {{0, 0}, {0, block_size}, {0, 2 * block_size}, {0, 3 * block_size}},
                    {{0, 0}, {block_size, 0}, {2 * block_size, 0}, {3 * block_size, 0}},
                    {{0, 0}, {0, block_size}, {0, 2 * block_size}, {0, 3 * block_size}}
            },

            {
                    {{0, 0}, {block_size, 0}, {2 * block_size, 0}, {2 * block_size, block_size}},
                    {{0, 2 * block_size}, {block_size, 0}, {block_size, block_size}, {block_size, 2 * block_size}},
                    {{0, 0}, {0, block_size}, {block_size, block_size}, {2 * block_size, block_size}},
                    {{block_size, 0}, {2 * block_size, 0}, {block_size, block_size}, {block_size, 2 * block_size}}
            },

            {
                    {{0, 0}, {block_size, 0}, {2 * block_size, 0}, {0, block_size}},
                    {{0, 0}, {block_size, 0}, {block_size, block_size}, {block_size, 2 * block_size}},
                    {{0, block_size}, {2 * block_size, 0}, {block_size, block_size}, {2 * block_size, block_size}},
                    {{0, 0}, {0, block_size}, {0, 2 * block_size}, {block_size, 2 * block_size}}
            },

            {
                    {{0, 0}, {0, 0}, {0, 0}, {0, 0}},
                    {{0, 0}, {0, 0}, {0, 0}, {0, 0}},
                    {{0, 0}, {0, 0}, {0, 0}, {0, 0}},
                    {{0, 0}, {0, 0}, {0, 0}, {0, 0}}
            },

            {
                    {{0, block_size}, {block_size, 0}, {2 * block_size, 0}, {block_size, block_size}},
                    {{0, 0}, {0, block_size}, {block_size, block_size}, {block_size, 2 * block_size}},
                    {{0, block_size}, {block_size, 0}, {2 * block_size, 0}, {block_size, block_size}},
                    {{0, 0}, {0, block_size}, {block_size, block_size}, {block_size, 2 * block_size}}
            },

            {
                    {{0, 0}, {block_size, 0}, {2 * block_size, 0}, {block_size, block_size}},
                    {{0, block_size}, {block_size, 0}, {block_size, block_size}, {block_size, 2 * block_size}},
                    {{0, block_size}, {block_size, 0}, {block_size, block_size}, {2 * block_size, block_size}},
                    {{0, 0}, {0, block_size}, {block_size, block_size}, {0, 2 * block_size}}
            },

            {
                    {{0, 0}, {block_size, 0}, {block_size, block_size}, {2 * block_size, block_size}},
                    {{0, block_size}, {block_size, 0}, {block_size, block_size}, {0, 2 * block_size}},
                    {{0, 0}, {block_size, 0}, {block_size, block_size}, {2 * block_size, block_size}},
                    {{0, block_size}, {block_size, 0}, {block_size, block_size}, {0, 2 * block_size}}
            }
    };
    private int[][] lengths = {
            {4, 1, 4, 1},
            {3, 2, 3, 2},
            {3, 2, 3, 2},
            {0, 0, 0, 0},
            {3, 2, 3, 2},
            {3, 2, 3, 2},
            {3, 2, 3, 2}
    };
    private ArrayList<Point> move = new ArrayList<>();
    private ArrayList<Point> move_tmp = new ArrayList<>();
    private int type;
    private int cur_color;
    private int cur_len;
    private int left_edge;
    private int state;
    private boolean is_falling = false;

    Model() {}

    public int getScore() {
        return score;
    }

    public boolean isGame_over() {
        return game_over;
    }

    private void generate_obj() {
        Point p1, p2, p3, p4;
        Random r = new Random();
        int figure = r.nextInt(7);
        state = 0;
        switch (figure) {
            case 0: //палочка 4х1
                cur_color = 1;
                type = 0;
                p1 = new Point(size_x / 2 - 2 * block_size, 0);
                p2 = new Point(size_x / 2 - block_size, 0);
                p3 = new Point(size_x / 2, 0);
                p4 = new Point(size_x / 2 + block_size, 0);
                cur_obj.add(p1);
                cur_obj.add(p2);
                cur_obj.add(p3);
                cur_obj.add(p4);
                cur_len = 4 * block_size;
                left_edge = size_x / 2 - 2 * block_size;
                break;
            case 1: //буквка Г вправо
                cur_color = 2;
                type = 1;
                p1 = new Point(size_x / 2 - block_size, 0);
                p2 = new Point(size_x / 2, 0);
                p3 = new Point(size_x / 2 + block_size, 0);
                p4 = new Point(size_x / 2 + block_size, block_size);
                cur_obj.add(p1);
                cur_obj.add(p2);
                cur_obj.add(p3);
                cur_obj.add(p4);
                cur_len = 3 * block_size;
                left_edge = size_x / 2 - block_size;
                break;
            case 2: //буква Г влево
                cur_color = 3;
                type = 2;
                p1 = new Point(size_x / 2 - block_size, 0);
                p2 = new Point(size_x / 2, 0);
                p3 = new Point(size_x / 2 + block_size, 0);
                p4 = new Point(size_x / 2 - block_size, block_size);
                cur_obj.add(p1);
                cur_obj.add(p2);
                cur_obj.add(p3);
                cur_obj.add(p4);
                cur_len = 3 * block_size;
                left_edge = size_x / 2 - block_size;
                break;
            case 3: //квадрат
                cur_color = 4;
                type = 3;
                p1 = new Point(size_x / 2 - block_size, 0);
                p2 = new Point(size_x / 2, 0);
                p3 = new Point(size_x / 2 - block_size, block_size);
                p4 = new Point(size_x / 2, block_size);
                cur_obj.add(p1);
                cur_obj.add(p2);
                cur_obj.add(p3);
                cur_obj.add(p4);
                cur_len = 2 * block_size;
                left_edge = size_x / 2 - block_size;
                break;
            case 4: //буква S
                cur_color = 5;
                type = 4;
                p1 = new Point(size_x / 2 + block_size, 0);
                p2 = new Point(size_x / 2 - block_size, block_size);
                p3 = new Point(size_x / 2, 0);
                p4 = new Point(size_x / 2, block_size);
                cur_obj.add(p1);
                cur_obj.add(p2);
                cur_obj.add(p3);
                cur_obj.add(p4);
                cur_len = 3 * block_size;
                left_edge = size_x / 2 - block_size;
                break;
            case 5: //буква T
                cur_color = 6;
                type = 5;
                p1 = new Point(size_x / 2 - block_size, 0);
                p2 = new Point(size_x / 2, 0);
                p3 = new Point(size_x / 2 + block_size, 0);
                p4 = new Point(size_x / 2, block_size);
                cur_obj.add(p1);
                cur_obj.add(p2);
                cur_obj.add(p3);
                cur_obj.add(p4);
                cur_len = 3 * block_size;
                left_edge = size_x / 2 - block_size;
                break;
            case 6: //буква Z
                cur_color = 7;
                type = 6;
                p1 = new Point(size_x / 2 - block_size, 0);
                p2 = new Point(size_x / 2, 0);
                p3 = new Point(size_x / 2, block_size);
                p4 = new Point(size_x / 2 + block_size, block_size);
                cur_obj.add(p1);
                cur_obj.add(p2);
                cur_obj.add(p3);
                cur_obj.add(p4);
                cur_len = 3 * block_size;
                left_edge = size_x / 2 - block_size;
                break;
        }
    }

    private void place_obj() {
        for (Point p : cur_obj) {
            table[(int) p.getX()][(int) p.getY()] = cur_color;
        }
    }

    private void delete_obj() {
        for (Point p : cur_obj) {
            table[(int) p.getX()][(int) p.getY()] = 0;
        }
    }

    private void save_obj(ArrayList<Point> obj) {
        for (Point p : cur_obj) {
            obj.add((Point) p.clone());
        }
    }

    private void set_from_save(ArrayList<Point> obj) {
        for (Point p : obj)
            table[(int) p.getX()][(int) p.getY()] = cur_color;
    }

    public void step() {
        if (!is_falling) {
            is_falling = true;
            generate_obj();
            if (check_collisions(cur_obj)) {
                game_over = true;
                return;
            }
            place_obj();
            return;
        }
        save_obj(tmp);
        delete_obj();
        for (Point p : cur_obj) {
            p.move((int) p.getX(), (int) p.getY() + block_size);
        }
        if (check_collisions(cur_obj)) {
            is_falling = false;
            set_from_save(tmp);
            tmp.clear();
            cur_obj.clear();
            check_del();
            return;
        }
        tmp.clear();
        place_obj();

    }

    private void check_del() {
        int counter = 0;
        for (int i = 0; i < size_y; i += block_size) {
            for (int j = 0; j < size_x; j += block_size) {
                if (table[j][i] != 0)
                    counter++;
            }
            if (counter == size_x / block_size) {
                del_line(i);
                score++;
            }
            counter = 0;
        }
    }

    private void del_line(int line) {
        for (int i = line; i >= block_size; i -= block_size)
            for (int j = 0; j < size_x; j++)
                table[j][i] = table[j][i - block_size];
    }

    private boolean check_collisions(ArrayList<Point> obj) {
        for (Point p : obj) {
            if ((int) p.getY() == size_y)
                return true;
            if (table[(int) p.getX()][(int) p.getY()] != 0)
                return true;
        }
        return false;
    }

    private boolean check_side_collision(char dir) {
        switch (dir) {
            case 'l':
                if (left_edge == 0)
                    return true;
                for (Point p : cur_obj) {
                    if ((!cur_obj.contains(new Point((int) p.getX() - block_size, (int) p.getY())))
                            && (table[(int) p.getX() - block_size][(int) p.getY()] != 0))
                        return true;
                }
                break;
            case 'r':
                if (left_edge + cur_len == size_x)
                    return true;
                for (Point p : cur_obj) {
                    if ((!cur_obj.contains(new Point((int) p.getX() + block_size, (int) p.getY())))
                            && (table[(int) p.getX() + block_size][(int) p.getY()] != 0))
                        return true;
                }
                break;
        }
        return false;
    }

    public void move_left() {
        if (check_side_collision('l'))
            return;
        delete_obj();
        for (Point p : cur_obj) {
            p.move((int) p.getX() - block_size, (int) p.getY());
        }
        left_edge -= block_size;
        place_obj();
    }

    public void move_right() {
        if (check_side_collision('r'))
            return;
        delete_obj();
        for (Point p : cur_obj) {
            p.move((int) p.getX() + block_size, (int) p.getY());
        }
        left_edge += block_size;
        place_obj();
    }

    public void rotate() {
        try {
            Point tmp;
            if (type == 3)
                return;
            if (cur_obj.size() == 0)
                return;
            int pivotX, pivotY;
            pivotX = (int) cur_obj.get(0).getX();
            pivotY = (int) cur_obj.get(0).getY();
            if (pivotX > size_x - lengths[type][(state + 1) % 4] * block_size)
                pivotX = size_x - lengths[type][(state + 1) % 4] * block_size;
            for (int i = 0; i < 4; i++) {
                tmp = new Point(states[type][(state + 1) % 4][i][0] + pivotX,
                        states[type][(state + 1) % 4][i][1] + pivotY);
                move.add(tmp);
            }
            save_obj(move_tmp);
            delete_obj();
            if (check_collisions(move)) {
                set_from_save(move_tmp);
                return;
            }
            for (int i = 0; i < 4; i++) {
                cur_obj.get(i).move((int) move.get(i).getX(), (int) move.get(i).getY());
            }
            left_edge = (int) cur_obj.get(0).getX();
            place_obj();
            state++;
            cur_len = lengths[type][state % 4] * block_size;
        } finally {
            move.clear();
            move_tmp.clear();
        }
    }

    public int[][] getTable() {
        return table;
    }
}