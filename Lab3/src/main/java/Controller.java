import javafx.animation.PauseTransition;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class Controller {
    private View v;
    private Model model;
    private int delay = 300;
    private int score = 0;
    Controller(View view)
    {
        model = new Model();
        v = view;
    }
    public void new_game()
    {
        model = new Model();
    }
    public void start_game()
    {
        PauseTransition pt = new PauseTransition(Duration.millis(delay));
        pt.setOnFinished(e ->
        {
            if (!model.isGame_over()) {
                model.step();
                v.draw(model.getTable());
                score = model.getScore();
                v.setScore(score);
                pt.playFromStart();
            }
            else {
                v.game_over();
            }
        });
        pt.play();
        EventHandler<KeyEvent> a = e -> {
            KeyCode key = e.getCode();
            if (key.equals(KeyCode.UP)) {
                model.rotate();
                v.draw(model.getTable());
            }
            if (key.equals(KeyCode.DOWN))
            {
                if (!model.isGame_over()) {
                    model.step();
                    v.draw(model.getTable());
                }
            }
            if (key.equals(KeyCode.RIGHT)) {
                model.move_right();
                v.draw(model.getTable());
            }
            if (key.equals(KeyCode.LEFT)){
                model.move_left();
                v.draw(model.getTable());
            }
        };
        v.canvas.setOnKeyPressed(a);
    }
}
