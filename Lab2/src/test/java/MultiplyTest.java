import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultiplyTest {

    @Test
    void exec() {
        Multiply m = new Multiply("void");
        Context c = new Context();
        c.pushToStack(4);
        c.pushToStack(5);
        m.exec(c);
        double actual = c.getFromStack();
        double expected = 20;
        assertEquals(expected, actual);
    }
}