import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Context {
    private Stack<Double> stack;
    private Map<String, Double> definitionTable;
    Context()
    {
        stack = new Stack<>();
        definitionTable = new HashMap<>();
    }

    public void newDefinition(String key, double value) throws IllegalArgumentException
    {
        if (definitionTable.containsKey(key))
        {
            throw new IllegalArgumentException("key \"" + key + "\" was already defined");
        }
        definitionTable.put(key, value);
    }

    public boolean containsDefinition(String key)
    {
        return definitionTable.containsKey(key);
    }

    public double getByDefinition(String key)
    {
        return definitionTable.get(key);
    }

    public void pushToStack(double n)
    {
        stack.push(n);
    }

    public double getFromStack() throws EmptyStackException
    {
        return stack.pop();
    }

    public double peekFromStack() throws EmptyStackException
    {
        return stack.peek();
    }
}
