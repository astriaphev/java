import java.util.ArrayList;
import java.util.List;

public class Factory {
    public List<Command> generate(List<Creator> creators)
    {
        List<Command> flow = new ArrayList<>();
        for (Creator creator: creators)
        {
            Command command = creator.factoryMethod();
            flow.add(command);
        }
        return flow;
    }
}