import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

class Token{
    private String command;
    private String arg;
    Token(String c, String a)
    {
        command = c;
        arg = a;
    }
    public String getCommand()
    {
        return command;
    }
    public String getArg()
    {
        return arg;
    }
}

public class Parser {
    private InputStreamReader reader;

    Parser(InputStreamReader r) {
        reader = r;
    }

    public List<Token> parseCommands() throws IOException {
        String line;
        String command;
        String arg;
        List<Token> tokenList = new ArrayList<>();
        Scanner scanner = new Scanner(reader);
        int len;
        while (scanner.hasNext()) {
            line = new String(scanner.nextLine());
            if (line.equals("exit"))
                break;
            String[] words = line.split(" ");
            command = words[0];
            if (command.equals("#"))
                continue;
            arg = "void";
            len = words.length;
            if (len > 3)
                throw new IOException("Too many arguments");
            if (len != 1) {
                arg = words[1];
                if (len == 3)
                    arg = arg.concat(" " + words[2]);
            }
            Token token = new Token(command, arg);
            tokenList.add(token);
        }
        return tokenList;
    }
}