import java.util.List;

public class Executor {
    void execute(Context c, List<Command> flow) throws IllegalArgumentException
    {
        for (Command command: flow)
        {
                command.exec(c);
        }
    }
}
