import java.io.*;
import java.util.List;
import org.apache.log4j.Logger;

public class main {
    private static final Logger logger = Logger.getLogger(main.class);
    public static void main(String[] args)
    {
        boolean fatal = false;
        Parser p;
        List<Token> tokenList = null;
        InputStreamReader reader;
        if (args.length == 1)
        {
            try{
                reader = new InputStreamReader(new FileInputStream(args[0]));
            }
            catch (FileNotFoundException e)
            {
                logger.warn("File " + args[0] +" not found : System input stream will be used instead," +
                        " type \"exit\" when you want to stop entering commands");
                reader = new InputStreamReader(System.in);
            }
        }
        else
        {
            reader = new InputStreamReader(System.in);
            logger.warn("System input stream will be used," +
                    " type \"exit\" when you want to stop entering commands");
        }
        p = new Parser(reader);
        try {
            tokenList = p.parseCommands();
        }
        catch (IOException e)
        {
            logger.fatal("");
            fatal = true;
            System.err.println("Error: " + e.getLocalizedMessage());
        }
        finally {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace(System.err);
                }
        }
        if (fatal)
            System.exit(-1);
        List<Creator> creators;
        List<Command> flow;
        try {
            TokenListToCreators a = new TokenListToCreators();
            creators = a.convert(tokenList);
            Factory f = new Factory();
            flow = f.generate(creators);
            Context context = new Context();
            Executor e = new Executor();
            e.execute(context, flow);
        }
        catch (IllegalArgumentException|UnsupportedOperationException e)
        {
            logger.fatal("");
            fatal = true;
            System.err.println("Error: " + e.getLocalizedMessage());
        }
        if (fatal)
            System.exit(-2);
    }
}