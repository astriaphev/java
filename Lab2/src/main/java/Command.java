import java.util.EmptyStackException;

public abstract class Command {
    abstract void exec(Context context);
}

class Push extends Command {
    private String arg;
    Push(String arg)
    {
        this.arg = arg;
    }
    private boolean checkArg(String arg)
    {
        String[] words;
        words = arg.split(" ");
        return words.length == 1;
    }
    public void exec(Context context)
    {
        try {
            double v;
            if (!checkArg(arg))
                throw new IllegalArgumentException("Illegal argument");
            if (!context.containsDefinition(arg))
            {
                v = Double.parseDouble(arg);
            }
            else
                v = context.getByDefinition(arg);
            context.pushToStack(v);
        }
        catch (NumberFormatException e)
        {
            throw new IllegalArgumentException("Illegal argument");
        }
    }
}

class Pop extends Command {
    private String arg;
    Pop(String arg)
    {
        this.arg = arg;
    }
    private boolean checkArg(String arg)
    {
        return arg.equals("void");
    }
    public void exec(Context context)
    {
        if (!checkArg(arg))
            throw new IllegalArgumentException();
        try {
            context.getFromStack();
        }
        catch (EmptyStackException e) {
            throw new UnsupportedOperationException("Couldn't execute operation - stack is empty");
        }
    }
}

class Plus extends Command {
    private String arg;
    Plus(String arg)
    {
        this.arg = arg;
    }
    private boolean checkArg(String arg)
    {
        return arg.equals("void");
    }
    public void exec(Context context)
    {
        if (!checkArg(arg))
            throw new IllegalArgumentException("Wrong argument for operation");
        double a,b;
        try {
            a = context.getFromStack();
            b = context.getFromStack();
            b = b + a;
            context.pushToStack(b);
        }
        catch (EmptyStackException e)
        {
            throw new UnsupportedOperationException("Couldn't execute operation - stack is empty");
        }
    }
}

class Minus extends Command {
    private String arg;
    Minus(String arg)
    {
        this.arg = arg;
    }
    private boolean checkArg(String arg)
    {
        return arg.equals("void");
    }
    public void exec(Context context)
    {
        if (!checkArg(arg))
            throw new IllegalArgumentException("Wrong argument for operation");
        double a,b;
        try {
            a = context.getFromStack();
            b = context.getFromStack();
            b = b - a;
            context.pushToStack(b);
        }
        catch (EmptyStackException e)
        {
            throw new UnsupportedOperationException("Couldn't execute operation - stack is empty");
        }
    }
}

class Multiply extends Command {
    private String arg;
    Multiply(String arg)
    {
        this.arg = arg;
    }
    private boolean checkArg(String arg)
    {
        return arg.equals("void");
    }
    public void exec(Context context)
    {
        if (!checkArg(arg))
            throw new IllegalArgumentException("Wrong argument for operation");
        double a,b;
        try {
            a = context.getFromStack();
            b = context.getFromStack();
            b = b * a;
            context.pushToStack(b);
        }
        catch (EmptyStackException e)
        {
            throw new UnsupportedOperationException("Couldn't execute operation - stack is empty");
        }
    }
}

class Divide extends Command {
    private String arg;
    Divide(String arg)
    {
        this.arg = arg;
    }
    private boolean checkArg(String arg)
    {
        return arg.equals("void");
    }
    public void exec(Context context)
    {
        if (!checkArg(arg))
            throw new IllegalArgumentException("Wrong argument for operation");
        double a,b;
        try {
            a = context.getFromStack();
            b = context.getFromStack();
            b = b / a;
            if (Double.isInfinite(b))
                throw new UnsupportedOperationException("Division by zero occurred");
            context.pushToStack(b);
        }
        catch (EmptyStackException e)
        {
            throw new UnsupportedOperationException("Couldn't execute operation - stack is empty");
        }
    }
}

class Sqrt extends Command {
    private String arg;
    Sqrt(String arg)
    {
        this.arg = arg;
    }
    private boolean checkArg(String arg)
    {
        return arg.equals("void");
    }
    public void exec(Context context)
    {
        if (!checkArg(arg))
            throw new IllegalArgumentException("Wrong argument for operation");
        double v;
        try {
            v = context.getFromStack();
            v = Math.sqrt(v);
            context.pushToStack(v);
        }
        catch (EmptyStackException e)
        {
            throw new UnsupportedOperationException("Couldn't execute operation - stack is empty");
        }
    }
}

class Print extends Command {
    private String arg;
    Print(String arg)
    {
        this.arg = arg;
    }
    private boolean checkArg(String arg)
    {
        return arg.equals("void");
    }
    public void exec(Context context)
    {
        if (!checkArg(arg))
            throw new IllegalArgumentException("Wrong argument for operation");
        double v;
        try{
            v = context.peekFromStack();
            System.out.println(v);
        }
        catch (EmptyStackException e)
        {
            throw new UnsupportedOperationException("Couldn't execute operation - stack is empty");
        }
    }
}

class Define extends Command {
    private String arg;
    private String key;
    private double val;
    Define(String arg)
    {
        this.arg = arg;
    }
    private boolean checkArg(String arg) throws NumberFormatException
    {
        if (arg.length() == 0)
            return false;
        String[] words;
        String delimiter = " ";
        words = arg.split(delimiter);
        if (words.length != 2)
            return false;
        for (int i = 0; i < words[0].length(); i++)
        {
            if (!Character.isLetter(words[0].charAt(i)))
                return false;
        }
        key = words[0];
        val = Double.parseDouble(words[1]);
        return true;
    }
    public void exec(Context context) throws IllegalArgumentException
    {
        try {
            if (!checkArg(arg))
                throw new IllegalArgumentException("Illegal definition");
        }
        catch (NumberFormatException e){
            throw new IllegalArgumentException("Illegal definition - non-double value");
        }
        context.newDefinition(key, val);
    }
}