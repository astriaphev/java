public abstract class Creator {
    public abstract Command factoryMethod();
}

class PushCreator extends Creator {
    private String arg;
    PushCreator(String a) {
        arg = a;
    }

    @Override
    public Command factoryMethod() { return new Push(arg); }
}

class PopCreator extends Creator {
    private String arg;
    PopCreator(String a) {
        arg = a;
    }

    @Override
    public Command factoryMethod() { return new Pop(arg); }
}

class PlusCreator extends Creator {
    private String arg;
    PlusCreator(String a) {
        arg = a;
    }

    @Override
    public Command factoryMethod() { return new Plus(arg); }
}

class MinusCreator extends Creator {
    private String arg;
    MinusCreator(String a) {
        arg = a;
    }

    @Override
    public Command factoryMethod() { return new Minus(arg); }
}


class MultiplyCreator extends Creator {
    private String arg;
    MultiplyCreator(String a) {
        arg = a;
    }

    @Override
    public Command factoryMethod() { return new Multiply(arg); }
}

class DivideCreator extends Creator {
    private String arg;
    DivideCreator(String a) {
        arg = a;
    }

    @Override
    public Command factoryMethod() { return new Divide(arg); }
}

class SqrtCreator extends Creator {
    private String arg;
    SqrtCreator(String a) {
        arg = a;
    }

    @Override
    public Command factoryMethod() { return new Sqrt(arg); }
}

class PrintCreator extends Creator {
    private String arg;
    PrintCreator(String a) {
        arg = a;
    }

    @Override
    public Command factoryMethod() { return new Print(arg); }
}

class DefineCreator extends Creator {
    private String arg;
    DefineCreator(String a) {
        arg = a;
    }

    @Override
    public Command factoryMethod() { return new Define(arg); }
}
