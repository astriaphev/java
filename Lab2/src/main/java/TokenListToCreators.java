import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

class GetCreator{
    private Map<String, Class> hashMap = new HashMap<>();
    GetCreator()  throws ClassNotFoundException, IOException, NullPointerException
    {
        Properties property = new Properties();
            InputStream inputStream =
                    main.class.getClassLoader().getResourceAsStream("config.properties");

            property.load(inputStream);
            String[] commands = {"PUSH", "POP", "+", "-", "*", "*", "/", "SQRT", "PRINT", "DEFINE"};
            for (String command : commands)
            {
                hashMap.put(command, Class.forName(property.getProperty(command)));
            }
    }
    public Creator get(Token token) throws ClassNotFoundException, UnsupportedOperationException, SecurityException
    {
        String command = token.getCommand();
        String arg = token.getArg();
        Creator obj;
        try {
            if (!hashMap.containsKey(command))
                throw new UnsupportedOperationException(command + " - no such operation found");
            obj =  (Creator)hashMap.get(command).getDeclaredConstructor(new Class[]{String.class}).newInstance(arg);
        }
        catch (InstantiationException|IllegalAccessException|NoSuchMethodException|
                InvocationTargetException|SecurityException e) {
            throw new ClassNotFoundException();
        }
        return obj;
    }
}
public class TokenListToCreators {
    public List<Creator> convert(List<Token> tokenList) throws UnsupportedOperationException
    {
        List<Creator> creators = new ArrayList<>();
        try {
            GetCreator c = new GetCreator();
            for (Token curToken : tokenList)
            {
                Creator obj = c.get(curToken);
                creators.add(obj);
            }
        }
        catch (ClassNotFoundException|IOException|NullPointerException e)
        {
            throw new UnsupportedOperationException("Couldn't resolve properties file, check conf.properties");
        }
        return creators;
    }
}