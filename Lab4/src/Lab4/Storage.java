package Lab4;

import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.util.ArrayList;

public class Storage {
    private ArrayList<Item> s;
    private int capacity;
    private int currently;
    private int overall;
    private int requested;
    private IntegerProperty currentlyProperty;
    private IntegerProperty overallProperty;
    Storage(int c) {
        capacity = c;
        s = new ArrayList<>();
        currently = 0;
        overall = 0;
        requested = 0;
        currentlyProperty = new SimpleIntegerProperty(0);
        overallProperty = new SimpleIntegerProperty(0);
    }
    public synchronized void requestCar() {
        requested++;
        notifyAll();
    }
    public synchronized int getRequested() { return requested; }
    public synchronized void setRequested(int r) { requested = r; }
    public synchronized Car getCar() {
        Car i = (Car)s.remove(0);
        this.notifyAll();
        currently--;
        Platform.runLater(() -> currentlyProperty.setValue(currently));
        return i;
    }
    public boolean space_available() {
        return !(s.size() == capacity);
    }
    public boolean available() { return !s.isEmpty();}
    public int needed() { return capacity - s.size();}
    public synchronized void store(Item it) {
        s.add(it);
        currently++;
        overall++;
        Platform.runLater(() -> currentlyProperty.setValue(currently));
        Platform.runLater(() -> overallProperty.setValue(overall));
        if (available())
            this.notifyAll();
    }
    public synchronized Item release() {
        Item i = s.remove(s.size() - 1);
        if (space_available()) this.notifyAll();
        currently--;
        Platform.runLater(() -> currentlyProperty.setValue(currently));
        return i;
    }
    public IntegerProperty getCurrentlyProperty() { return currentlyProperty; }
    public IntegerProperty getOverallProperty() { return overallProperty; }
}