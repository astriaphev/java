package Lab4;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class EngineSupplier extends Thread {
    private boolean processing = true;
    private final Storage storage;
    private DoubleProperty delayProperty = new SimpleDoubleProperty(0.8);
    EngineSupplier(Storage s) {
        storage = s;
    }
    public DoubleProperty getDelayProperty() { return delayProperty; }
    public void setRunning(boolean s) {
        processing = s;
    }
    @Override
    public void run() {
        Item engine;
        int counter = 1;
        while (processing)
        {
            try {Thread.sleep(Math.round(delayProperty.getValue() * 1000));}
            catch (InterruptedException e){
                e.printStackTrace();
            }
            engine = new Item();
            engine.setID("E" + counter);
            counter++;
            synchronized (storage) {
                try {
                    while (!storage.space_available()) {
                        storage.wait();
                    }
                    if (storage.space_available()) {
                        storage.store(engine);
                    }
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
