package Lab4;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    private Factory f = new Factory();
    @FXML
    Label producedLabel, carsInStorageLabel, assemblingLabel, bodiesLabel, enginesLabel, accessoryLabel,
            dealerDelayLabel, esDelayLabel, bsDelayLabel, asDelayLabel;
    @FXML
    Slider dealerDelay, esDelay, bsDelay, asDelay;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            f.initiate();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        producedLabel.textProperty().bind(f.getOverall().asString());
        carsInStorageLabel.textProperty().bind(f.getCurrently().asString());
        assemblingLabel.textProperty().bind(f.getAssembling().asString());
        bodiesLabel.textProperty().bind(f.getBodies().asString());
        enginesLabel.textProperty().bind(f.getEngines().asString());
        accessoryLabel.textProperty().bind(f.getAccessory().asString());

        dealerDelayLabel.textProperty().bind(dealerDelay.valueProperty().asString());
        dealerDelay.valueProperty().addListener((obs, oldVal, newVal) ->
               dealerDelay.setValue(newVal.doubleValue()));
        f.getDealerDelay().bind(dealerDelay.valueProperty());

        esDelayLabel.textProperty().bind(esDelay.valueProperty().asString());
        esDelay.valueProperty().addListener((obs, oldVal, newVal) ->
                esDelay.setValue(newVal.doubleValue()));
        f.getEnSupDelay().bind(esDelay.valueProperty());

        bsDelayLabel.textProperty().bind(bsDelay.valueProperty().asString());
        bsDelay.valueProperty().addListener((obs, oldVal, newVal) ->
                bsDelay.setValue(newVal.doubleValue()));
        f.getBoSupDelay().bind(bsDelay.valueProperty());

        asDelayLabel.textProperty().bind(asDelay.valueProperty().asString());
        asDelay.valueProperty().addListener((obs, oldVal, newVal) ->
                asDelay.setValue(newVal.doubleValue()));
        f.getAcSupDelay().bind(asDelay.valueProperty());
    }
}