package Lab4;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class Factory {
    private BodySupplier bs;
    private EngineSupplier es;
    private AccessorySupplier as;
    private ArrayList<Dealer> d;
    private Storage bodyStorage, engineStorage, accessoryStorage, carStorage;
    private CarStorageController csc;
    DoubleProperty dealerDelayPropery;
    public void initiate() throws IOException {
        Properties property = new Properties();
        FileInputStream inputStream = new FileInputStream("C:\\Users\\Артем\\Desktop\\JavaLabs\\Lab4\\src\\Lab4\\config.properties");
        property.load(inputStream);
        bodyStorage = new Storage(Integer.parseInt(property.getProperty("BodyStorageCapacity")));
        engineStorage = new Storage(Integer.parseInt(property.getProperty("EngineStorageCapacity")));
        accessoryStorage = new Storage(Integer.parseInt(property.getProperty("AccessoryStorageCapacity")));
        carStorage = new Storage(Integer.parseInt(property.getProperty("CarStorageCapacity")));
        bs = new BodySupplier(bodyStorage);
        es = new EngineSupplier(engineStorage);
        as = new AccessorySupplier(accessoryStorage);
        bs.start();
        es.start();
        as.start();
        int d_num = Integer.parseInt(property.getProperty("Dealers"));
        Dealer tmp;
        d = new ArrayList<>();
        dealerDelayPropery = new SimpleDoubleProperty(3);
        for (int i = 1; i <= d_num; i++) {
            tmp = new Dealer(i, carStorage, dealerDelayPropery);
            tmp.start();
            d.add(tmp);
        }
        csc = new CarStorageController(Integer.parseInt(property.getProperty("Workers")),
                bodyStorage, engineStorage, accessoryStorage, carStorage);
        csc.start();
    }
    public IntegerProperty getOverall() { return carStorage.getOverallProperty(); }
    public IntegerProperty getCurrently() { return carStorage.getCurrentlyProperty(); }
    public IntegerProperty getAssembling() { return csc.getAssemblingProperty(); }
    public IntegerProperty getBodies() { return bodyStorage.getOverallProperty(); }
    public IntegerProperty getEngines() { return engineStorage.getOverallProperty(); }
    public IntegerProperty getAccessory() { return accessoryStorage.getOverallProperty(); }

    public DoubleProperty getDealerDelay() { return dealerDelayPropery; }
    public DoubleProperty getEnSupDelay() { return es.getDelayProperty(); }
    public DoubleProperty getBoSupDelay() { return bs.getDelayProperty(); }
    public DoubleProperty getAcSupDelay() { return as.getDelayProperty(); }


}