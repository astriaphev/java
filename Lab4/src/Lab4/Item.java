package Lab4;

public class Item {
    String id;
    public void setID(String new_id) { id = new_id; }

    public String getID() {
        return id;
    }
}
class Car extends Item {
    private Item body, engine, accessory;
    Car(Item b, Item e, Item a) {
        body = b;
        engine = e;
        accessory = a;
    }
    public String getFullInfo()
    {
        return "Body:<" + body.getID() + ">,Engine:<" + engine.getID() + ">,Accessory:<" + accessory.getID() + ">";
    }
}