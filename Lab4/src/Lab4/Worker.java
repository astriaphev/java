package Lab4;

public class Worker implements Runnable {
    private final Storage bodyStorage;
    private final Storage engineStorage;
    private final Storage accessoryStorage;
    private final Storage carStorage;
    private int serial;
    private int delay;
    Worker(Storage bs, Storage es, Storage as, Storage cs, int s) {
        bodyStorage = bs;
        engineStorage = es;
        accessoryStorage = as;
        carStorage = cs;
        serial = s;
        delay = 100;
    }
    private Item getItem(Storage s)
    {
        synchronized (s) {
            try {
                while (!s.available())
                    s.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return s.release();
        }
    }
    @Override
    public void run() {
        Item body, engine, accessory;
        Car car;
        body = getItem(bodyStorage);
        engine = getItem(engineStorage);
        accessory = getItem(accessoryStorage);
        try { Thread.sleep(delay); }
        catch (InterruptedException e) { e.printStackTrace();}
        car = new Car(body, engine, accessory);
        car.setID("V" + serial);
        synchronized (carStorage) {
            try {
                while (!carStorage.space_available())
                    carStorage.wait();
                if (carStorage.space_available())
                    carStorage.store(car);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Car with id: " + car.getID() + " was made " + "Full data: "
                + car.getFullInfo() + "made by " + Thread.currentThread().getName());
    }
}
