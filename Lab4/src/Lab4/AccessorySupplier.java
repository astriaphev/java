package Lab4;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class AccessorySupplier extends Thread {
    private boolean processing = true;
    private final Storage storage;
    private DoubleProperty delayProperty = new SimpleDoubleProperty(0.8);
    AccessorySupplier(Storage s) {
        storage = s;
    }
    public DoubleProperty getDelayProperty() { return delayProperty; }
    public void setRunning(boolean s) {
        processing = s;
    }
    @Override
    public void run() {
        Item accessory;
        int counter = 1;
        while (processing)
        {
            try {Thread.sleep(Math.round(delayProperty.getValue() * 1000));}
            catch (InterruptedException e){
                e.printStackTrace();
            }
            accessory = new Item();
            accessory.setID("A" + counter);
            counter++;
            synchronized (storage) {
                try {
                    while (!storage.space_available()) {
                        storage.wait();
                    }
                    if (storage.space_available()) {
                        storage.store(accessory);
                    }
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

/*
package Lab4;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class AccessorySupplier extends Thread {
    private boolean processing = true;
    private final Storage storage;
    private IntegerProperty delayProperty = new SimpleIntegerProperty(600);
    AccessorySupplier(Storage s) {
        storage = s;
    }
    public IntegerProperty getDelayProperty() { return delayProperty; }
    public void setRunning(boolean s) {
        processing = s;
    }
    @Override
    public void run() {
        Item accessory;
        int counter = 1;
        while (processing)
        {
            try {Thread.sleep(delayProperty.getValue());}
            catch (InterruptedException e){
                e.printStackTrace();
            }
            accessory = new Item();
            accessory.setID("A" + counter);
            counter++;
            synchronized (storage) {
                try {
                    while (!storage.space_available()) {
                        storage.wait();
                    }
                    if (storage.space_available()) {
                        storage.store(accessory);
                    }

                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
*/
