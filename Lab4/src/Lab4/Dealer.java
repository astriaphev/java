package Lab4;

import javafx.beans.property.DoubleProperty;

public class Dealer extends Thread {
    private boolean processing = true;
    private int id;
    private DoubleProperty delayProperty;
    private final Storage carStorage;
    private Car car;
    public void setRunning(boolean s) {
        processing = s;
    }
    Dealer(int id, Storage cs, DoubleProperty d) {
        this.id = id;
        carStorage = cs;
        delayProperty = d;
    }
    private void logSold() {
        System.out.println("Car with id: " + car.getID() + " sold by Dealer " + id
                + "\n                   Full data: " + car.getFullInfo());
    }
    @Override
    public void run() {
        while (processing)
        {
            try {
                Thread.sleep(Math.round(delayProperty.getValue() * 1000));
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (carStorage) {
                if (carStorage.available()) {
                    car = carStorage.getCar();
                    logSold();
                    continue;
                }
                carStorage.requestCar();
                System.out.println("requested");
            }
        }
    }
}
