package Lab4;

import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class CarStorageController extends Thread {
    private int count = 1;
    private boolean processing = true;
    private ExecutorService pool;
    private final Storage bodyStorage;
    private final Storage engineStorage;
    private final Storage accessoryStorage;
    private final Storage carStorage;
    private IntegerProperty assemblingProperty;
    public IntegerProperty getAssemblingProperty() { return assemblingProperty; }
    public void setRunning(boolean s) {
        processing = s;
    }
    CarStorageController(int nThreads, Storage bs, Storage es, Storage as, Storage cs) {
        pool = Executors.newFixedThreadPool(nThreads);
        bodyStorage = bs;
        engineStorage = es;
        accessoryStorage = as;
        carStorage = cs;
        assemblingProperty = new SimpleIntegerProperty(0);
    }
    @Override
    public void run() {
        ThreadPoolExecutor ex = (ThreadPoolExecutor)pool;
        int requested = 0;
        int i;
        try {
            synchronized (carStorage)
            {
                while (processing) {
                    carStorage.wait();
                    Platform.runLater(() -> assemblingProperty.setValue(ex.getQueue().size()));
                    if (carStorage.getRequested() <= ex.getQueue().size()) {
                        System.out.println("Requested "+ carStorage.getRequested() +" queue " +ex.getQueue().size());
                        continue;
                    }
                    while (!carStorage.space_available())
                        carStorage.wait();
                    requested = carStorage.getRequested();
                    for (i = 0; i < requested; i++) {
                        pool.submit(new Worker(bodyStorage, engineStorage, accessoryStorage, carStorage, count));
                        count++;
                        System.out.println("submitted");
                    }
                    System.out.println("Requested "+requested +" i " + i);
                    carStorage.setRequested(requested - i);
                }
                pool.shutdown();
            }
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
