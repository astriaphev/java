import java.io.*;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.*;


public class main {
    public static void main(String[] args)
    {
        Reader reader = null;
        Writer writer = null;
        char buf = '.';
        StringBuilder str = new StringBuilder();
        String a = new String("");
        Map<String, Integer> hashMap = new HashMap<>();
        try
        {
            reader = new InputStreamReader(new FileInputStream("in.txt"));
            while(reader.ready()) {
                while (buf != ' ' && buf != '\n') {
                    buf = (char) reader.read();
                    if (!Character.isLetter(buf)) {
                        if(str.toString().equals(a))
                            continue;
                        break;
                    }
                    str.append(buf);
                }
                if(str.toString().equals(a)) {
                    buf = '.';
                    continue;
                }
                if (!hashMap.containsKey(str.toString()))
                    hashMap.put(str.toString(), 1);
                else hashMap.put(str.toString(), hashMap.get(str.toString()) + 1);
                buf = '.';
                str.setLength(0);
            }
            List<Map.Entry<String, Integer>> list = new ArrayList<>(hashMap.entrySet());
            list.sort((o1, o2) -> o2.getValue().compareTo(o1.getValue()));

            double total = 0;
            for (Map.Entry<String, Integer> e : list) {
                System.out.println(e.getKey() + " : " + e.getValue());
                total += e.getValue();
            }
            writer = new OutputStreamWriter(new FileOutputStream("out.csv"));
            writer.write("Word"+','+"Frequency"+','+"Frequency %"+"\n");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator('.');
            NumberFormat formatter = new DecimalFormat("#0.000", symbols);
            String s = new String(list.get(0).getKey());
            for (Map.Entry<String, Integer> e : list)
                writer.write(e.getKey()+','+ e.getValue() +',' + formatter.format(e.getValue()*100/total) +"\n");
            writer.flush();
        }
        catch (IOException e)
        {
            System.err.println("Error while reading file:"+ e.getLocalizedMessage());
        }
        finally {
            if (null != reader) {
                try {
                    reader.close();
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            }
        }
    }
}
